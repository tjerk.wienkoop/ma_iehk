# first neural network with keras tutorial
import matplotlib.pyplot as plt
import numpy as np
from keras import metrics
from keras.callbacks import EarlyStopping
from keras.models import Sequential
from sklearn.model_selection import train_test_split
from tensorflow.keras import layers
from tensorflow.keras import regularizers


# load the dataset an randomize lines
dataset = np.loadtxt('dataset.csv', delimiter=';', skiprows=1)
np.random.shuffle(dataset)

# add noise to Aluminum column
# 1 is the mean of the normal distribution you are choosing from
# 0.05 is the standard deviation of the normal distribution
# 17315 is the number of elements you get in array noise
noise = np.random.normal(1,0.05,17315)
dataset[:,6] = dataset[:,6]*noise

# split into input (X) and output (y) variables
X = dataset[:,0:6]
y = dataset[:,6]

# split input and output into training and test datasets 
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.25)
X_val = X_train[-1300:]
y_val = y_train[-1300:]
X_train = X_train[:-1300]
y_train = y_train[:-1300]

# root mean squared error (rmse) for regression
def rmse(y_true, y_pred):
    from keras import backend
    return backend.sqrt(backend.mean(backend.square(y_pred - y_true), axis=-1))

# mean squared error (mse) for regression
def mse(y_true, y_pred):
    from keras import backend
    return backend.mean(backend.square(y_pred - y_true), axis=-1)

# coefficient of determination (R^2) for regression
def r_square(y_true, y_pred):
    from keras import backend as K
    SS_res =  K.sum(K.square(y_true - y_pred))
    SS_tot = K.sum(K.square(y_true - K.mean(y_true)))
    return (1 - SS_res/(SS_tot + K.epsilon()))

def r_square_loss(y_true, y_pred):
    from keras import backend as K
    SS_res =  K.sum(K.square(y_true - y_pred))
    SS_tot = K.sum(K.square(y_true - K.mean(y_true)))
    return 1 - ( 1 - SS_res/(SS_tot + K.epsilon()))

# define the keras model
model = Sequential([
    layers.Dense(40, input_dim=6, activation='relu'),
    layers.Dense(120, activation='relu'),
    layers.Dense(240, activation='relu'),
    layers.Dense(480, activation='relu'),
    layers.Dense(960, activation='relu'),
    layers.Dropout(.2),
    layers.Dense(480, activation='relu'),
    layers.Dense(240, activation='relu'),
    layers.Dense(120, activation='relu'),
    layers.Dense(60, activation='relu'),
    layers.Dense(1, activation='relu')
])

# compile the keras model
model.compile(loss="mean_absolute_error", optimizer='adam', metrics=["mean_squared_error", rmse, r_square])

# enable early stopping
earlystopping=EarlyStopping(monitor="mean_squared_error", patience=20, verbose=1, mode='auto')
# fit the keras model on the dataset
result = model.fit(X_train, y_train, epochs=1000, batch_size=15, validation_data=(X_val, y_val), callbacks=[earlystopping])

# Evaluate the model on the test data using `evaluate`
print("Evaluate on test data")
results = model.evaluate(X_test, y_test, batch_size=20)
print("test loss, test prec:", results)

# Generate predictions (probabilities -- the output of the last layer)
# on new data using `predict`
print("Generate predictions")
y_pred = model.predict(X_test)

# show the inputs and predicted outputs
for i in range(1,10):
    print("X=%s, Predicted=%s" % (inn_y_pred[i], y_pred[i]))

# -----------------------------------------------------------------------------
# print statistical figures of merit
# -----------------------------------------------------------------------------

import sklearn.metrics, math

print("\n")
print("Mean absolute error (MAE):      %f" % sklearn.metrics.mean_absolute_error(y_test,y_pred))
print("Mean squared error (MSE):       %f" % sklearn.metrics.mean_squared_error(y_test,y_pred))
print("Root mean squared error (RMSE): %f" % math.sqrt(sklearn.metrics.mean_squared_error(y_test,y_pred)))
print("R square (R^2):                 %f" % sklearn.metrics.r2_score(y_test,y_pred))

# plot training curve for mse
plt.plot(result.history['rmse'])
plt.plot(result.history['val_rmse'])
plt.title('rmse')
plt.ylabel('rmse')
plt.xlabel('epoch')
plt.legend(['train', 'test'], loc='upper left')
plt.show()

# print the linear regression and display datapoints
from sklearn.linear_model import LinearRegression
regressor = LinearRegression()
regressor.fit(y_test.reshape(-1, 1), y_pred)
y_fit = regressor.predict(y_pred)
reg_intercept = round(regressor.intercept_[0], 4)
reg_coef = round(regressor.coef_.flatten()[0], 4)
reg_label = "y = " + str(reg_intercept) + "*x +" + str(reg_coef)

plt.scatter(y_test, y_pred, color='blue', label='data')
plt.axis([0,1,0,1])
plt.plot(y_pred, y_fit, color='red', linewidth=2, label='Linear regression\n' + reg_label)
plt.title('Linear Regression')
plt.legend()
plt.xlabel('observed')
plt.ylabel('predicted')
plt.show()
model.save("FF_model")


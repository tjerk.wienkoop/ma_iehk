import numpy as np
import torch
import torch.utils.data
from sklearn.model_selection import train_test_split
import config as c


# load the dataset & randomize lines
dataset = np.loadtxt('Alles_Neu_norm.CSV', delimiter=';', skiprows=1)
np.random.shuffle(dataset)

X = dataset[:,0:6]
y = dataset[:,6]
# split input and output into training and test datasets
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.25)
X_val = X_train[-1300:]
y_val = y_train[-1300:]
X_train = X_train[:-1300]
y_train = y_train[:-1300]

x_test = torch.Tensor(X_test)
y_test = torch.Tensor(y_test)

x_train = torch.Tensor(X_train)

y_train = torch.Tensor(y_train)

x_train = torch.Tensor(x_train)
y_train = torch.Tensor(y_train)

c.test_loader = torch.utils.data.DataLoader(
    torch.utils.data.TensorDataset(x_test, y_test),
    batch_size=c.batch_size, shuffle=False, drop_last=True)

c.train_loader = torch.utils.data.DataLoader(
    torch.utils.data.TensorDataset(x_train, y_train),
    batch_size=c.batch_size, shuffle=True, drop_last=True)

c.test_time_functions = []#show_live_posteriors]

c.mmd_back_weighted = True

if __name__ == "__main__":
    import train

    c.lr_init = 3e-4
    c.final_decay = 0.2
    c.n_epochs = 60
    c.train_backward_mmd = False

    train.main()

    c.filename_in = c.filename_out
    c.filename_out += '_2'
    c.lr_init = 5e-5
    c.final_decay = 0.05
    c.n_epochs = 100
    c.train_backward_mmd = True

    train.main()

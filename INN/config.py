'''Global configuration for the experiments'''

######################
#  General settings  #
######################

# Filename to save the model under
filename_out    = 'INN_Model/ehla_inn.pt'
# Model to load and continue training. Ignored if empty string
filename_in     = ''
# Compute device to perform the training on, 'cuda' or 'cpu'
device          = 'cpu'
# Use interactive visualization of losses and other plots. Requires visdom
interactive_visualization = True
# Run a list of python functions at test time after each epoch
# See toy_modes_train.py for reference example
test_time_functions = []

#######################
#  Training schedule  #
#######################

# Initial learning rate
lr_init         = 5.0e-4
#Batch size
batch_size      = 20
# Total number of epochs to train for
n_epochs        = 100
# End the epoch after this many iterations (or when the train loader is exhausted)
n_its_per_epoch = 200
# For the first n epochs, train with a much lower learning rate. This can be
# helpful if the model immediately explodes.
pre_low_lr      = 1
# Decay exponentially each epoch, to final_decay*lr_init at the last epoch.
final_decay     = 0.02
# L2 weight regularization of model parameters
l2_weight_reg   = 1e-5
# Parameters beta1, beta2 of the Adam optimizer
adam_betas = (0.9, 0.95)

#####################
#  Data dimensions  #
#####################

ndim_x     = 6
ndim_pad_x = 0

ndim_y     = 1
ndim_z     = 5
ndim_pad_zy = 0

# Overwrite or import data loaders here.
# See dkfz_train.py for reference.
#from my_loaders import train_loader, test_loader

train_loader, test_loader = None, None

assert (ndim_x + ndim_pad_x 
        == ndim_y + ndim_z + ndim_pad_zy), "Dimensions don't match up"

############
#  Losses  #
############

train_forward_mmd    = True
train_backward_mmd   = True
train_reconstruction = True
train_max_likelihood = False

#Weights for losses
lambd_fit_forw       = 1.
lambd_mmd_forw       = 50.
lambd_reconstruct    = 1.
lambd_mmd_back       = 500.
lambd_max_likelihood = 1.

# Both for fitting, and for the reconstruction, perturb y with Gaussian 
# noise of this sigma
add_y_noise     = 4e-2
# For reconstruction, perturb z 
add_z_noise     = 2e-2
# In all cases, perturb the zero padding
add_pad_noise   = 1e-2

# For noisy forward processes, the sigma on y (assumed equal in all dimensions).
# This is only used if mmd_back_weighted of train_max_likelihoiod are True.
y_uncertainty_sigma = 0.12 * 4

mmd_forw_kernels = [(0.2, 2), (1.5, 2), (2.0, 2)]
mmd_back_kernels = [(0.1, 0.1), (0.8, 0.6), (0.2, 2)]
mmd_back_weighted = False

###########
#  Model  #
###########

# Initialize the model parameters from a normal distribution with this sigma
init_scale = 0.10
#
N_blocks   = 4
#
exponent_clamping = 4.0
#
hidden_layer_sizes = 128
#
use_permutation = True
#
verbose_construction = False

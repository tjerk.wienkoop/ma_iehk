% Liste aller .ctf Dateien im aktuellen Ordner oder Unterordnern
fds = fileDatastore('*.ctf', 'ReadFcn', @importdata);
fullFileNames = fds.Files;
numFiles = length(fullFileNames);

% Schleife über alle Dateien.
for k = 1 : numFiles

    fprintf('Now reading file %s\n', fullFileNames{k});

    %dateiname
    n = fullFileNames{k};
    [filepath,name,ext] = fileparts(fullFileNames{k});
    filename = convertCharsToStrings(name);
    mkdir([filename]);
    
    %Datei wird ausgelesen und CS erstellt
    %weitere Variablen im Workspace XY (XCells, YCells), kvwd (KV und Workingdistance)
    ebsd_cs_parse;
    build_cs;

    ebsd = EBSD.load(n,CS,'interface','ctf','convertEuler2SpatialReferenceFrame');
    
    switch k
        case 1 % Regionen für EHLA Studie 2400W
            region20 = [0 10 40 7.5]*10^1; %ROI AL-2
            region35 = [0 20 40 7.5]*10^1; %ROI AL-3.5
            region50 = [0 30 40 7.5]*10^1; %ROI AL-5
            region65 = [0 40 40 7.5]*10^1; %ROI AL-6.5
            region80 = [0 50 40 7.5]*10^1; %ROI AL-8
            %rectangle('position',region,'edgecolor','r','linewidth',2)
        case 2 % Regionen für EHLA Studie 2600W, mehrere nebeneinander möglich breite 150 statt 40           
            region20 = [0 15 150 7.5]*10^1; %ROI AL-2
            region35 = [0 25 150 7.5]*10^1; %ROI AL-3.5
            region50 = [0 35 150 7.5]*10^1; %ROI AL-5
            region65 = [0 45 150 7.5]*10^1; %ROI AL-6.5
            region80 = [0 55 150 7.5]*10^1; %ROI AL-8  
        case 3 % Regionen für EHLA Studie 2800W abklären           
            region20 = [0 16 80 7.5]*10^1; %ROI AL-2
            region35 = [0 26 80 7.5]*10^1; %ROI AL-3.5
            region50 = [0 36 80 7.5]*10^1; %ROI AL-5
            region65 = [0 46 80 7.5]*10^1; %ROI AL-6.5
            region80 = [0 56 80 7.5]*10^1; %ROI AL-8
        case 4 % Regionen für EHLA Studie 2800W abklären           
            region20 = [0 20 105 7.5]*10^1; %ROI AL-2
            region35 = [0 30 105 7.5]*10^1; %ROI AL-3.5
            region50 = [0 40 105 7.5]*10^1; %ROI AL-5
            region65 = [0 50 105 7.5]*10^1; %ROI AL-6.5
            region80 = [0 60 105 7.5]*10^1; %ROI AL-8
        case 5 % Regionen für EHLA Studie 3000W, mehrere nebeneinander möglich 100 statt 40           
            region20 = [0 15 100 7.5]*10^1; %ROI AL-2
            region35 = [0 25 100 7.5]*10^1; %ROI AL-3.5
            region50 = [0 35 100 7.5]*10^1; %ROI AL-5
            region65 = [0 45 100 7.5]*10^1; %ROI AL-6.5
            region80 = [0 55 100 7.5]*10^1; %ROI AL-8
        otherwise
            fprintf("no case found");
            return;
    end
    
    %% EBSD Regionen für EHLA Studie
    condition20 = inpolygon(ebsd,region20);
    condition35 = inpolygon(ebsd,region35);
    condition50 = inpolygon(ebsd,region50);
    condition65 = inpolygon(ebsd,region65);
    condition80 = inpolygon(ebsd,region80);
    
    ebsd_20 = ebsd(condition20);
    ebsd_35 = ebsd(condition35);
    ebsd_50 = ebsd(condition50);
    ebsd_65 = ebsd(condition65);
    ebsd_80 = ebsd(condition80);
    
    ebsds = ["20" "35" "50" "65" "80"];
    
    %Loop für jeden Bereich
    for l = 1 : length(ebsds)
        section = ebsds(l);
        regionname = strcat('ebsd_',section);
        current_ebsd = eval(regionname);
        

        %% CORRECTION

        ebsd_corrected=current_ebsd(current_ebsd.mad<3);
        % berechnet und indiziert Körner
        [grains,ebsd_corrected.grainId]=calcGrains(ebsd_corrected('indexed'),'angle',10*degree);
        % entfernt alle Körner <= 10
        ebsd_corrected(grains(grains.grainSize<=10))=[];

        %% smoothing

        F=halfQuadraticFilter;
        %
        ebsd_smoothed=smooth(ebsd_corrected('indexed'),'fill','filter',F);


        %% Körner berechnen
        [grains,ebsd_smoothed.grainId]=calcGrains(ebsd_smoothed('indexed'),'threshold',10*degree);

        %%

        outerBoundary_id=any(grains.boundary.grainId==0,2); % extract all grain boundaries with neighboring grain ids ==0,2
        grain_id = grains.boundary(outerBoundary_id).grainId;
        grain_id(grain_id==0)=[];
        %grains_new=grains;
        %grains_new(grain_id)=[]; %removes boundary grains.


        %% Grain Properties
        grain_area = grains.area;
        grain_ar = grains.aspectRatio;
        grain_size = grains.equivalentRadius*2;
        grain_shapefactor = grains.shapeFactor;
        gam = ebsd_smoothed.grainMean(ebsd_smoothed.KAM);

        %% CLD
        gbs= gb2gbs(grains.boundary);
        dxy = max(ebsd.unitCell) - min(ebsd.unitCell);
        spacing = min(dxy); %spacing between the test lines
        angs = 0:15:179; % number of angles
        cho_max = 0.5*max(grains.diameter); %maximum grain size comsidered in CLD
        stats = calc_CLD(gbs,spacing,angs,grains,'cho_max',cho_max);
        cld_full = transpose([stats.CLD;stats.CLD(1:end,:)]);
        %% speichern
        output_creator_ehla;
        
    end
    
    %% Aufräumen
    %clearvars -except fullFileNames numFiles fds region20 region35 region50 region65 region80;
end

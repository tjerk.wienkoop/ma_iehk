fprintf('Build CS Cell Array\n');
%%Vorformatierung
% Winkel Arrays aus Winkel und Namens Cell angleNames
% Ein Array enthält einen Winkel von jeder Phase. a1 den Ersten jeder Phase
a1 = cell2mat(angleNames(1));
a2 = cell2mat(angleNames(2));
a3 = cell2mat(angleNames(3));

% Phasennamen als Array
n1 = string(angleNames{1,4});

%%Laue Klasse definieren
%m-3m: fcc,bcc,NiAl,Cu,FeAl,chromite; 6mm: epsilon-martensit,Mg,Zn; mmm: Fe3C
laue = ["m-3m" "6mm" "mmm"];

%%Bauen von CS
CS ={1, m+1};
CS{1,1} = 'notIndexed';
%Iterieren über alle Phasen
for j = 1 : m
    %Auswahl der Laueklasse
    if a1(j) && a2(j) == a3(j)
        l = laue(1);
    elseif (a1(j) == a2(j))&& (a1(j) ~= a3(j))
        l = laue(2);
    else
        l = laue(3);
    end
    %Crystal Symmetry wird erstellt und in CS gespeichert
    CS{1,j+1} = crystalSymmetry(l, [round(a1(j),1) round(a2(j),1) round(a3(j),1)], 'mineral', n1(j) );
end

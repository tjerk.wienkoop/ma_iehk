% Induced yield strength (GPa): untere - oberes quartil : mean_value
% ys = normalisierter mean_value [0,1] mit 1 als minimum yield strength und
% 3 als maximum
switch k
    case 1
       % 2400W
       switch regionname
           case "ebsd_20"
            %Al-2:   1,65 - 1,79 : 1,72
            ys = [0.5733];
            %H = [32.48s];
            al = [0.1162];
            W = [0.800];
           case "ebsd_35"
            %Al-3.5: 1,89 - 2,01 : 1,95
            ys = [0.6500];
            %H = [27.88];
            al = [0.2997];
            W = [0.800];
           case "ebsd_50"
            %Al-5:   2,00 - 2,13 : 2,06
            ys = [0.6867];
            %H = [34.68];
            al = [0.5602];
            W = [0.800];
           case "ebsd_65"
            %Al-6.5: 2,15 - 2,28 : 2,21
            ys = [0.7367];
            %H = [40.79];
            al = [0.7003];
            W = [0.800];
           case "ebsd_80"
            %Al-8:   2,42 - 2,55 : 2,54
            ys = [0.8497];
            %H = [63.98];
            al = [1.00];
            W = [0.800];
       end
    case 2
       % 2600W
       switch regionname
           case "ebsd_20"
            %Al-2:   1,65 - 1,79 : 1,72
            ys = [0.5733];
            %H = [33.22];
            al = [0.0658];
            W = [0.866];
           case "ebsd_35"
            %Al-3.5: 1,85 - 1,95 : 1,90
            ys = [0.6333];
            %H = [31.13];
            al = [0.1555];
            W = [0.866];
           case "ebsd_50"
            %Al-5:   2,03 - 2,15 : 2,09
            ys = [0.6967];
            %H = [35.51];
            al = [0.3978];
            W = [0.866];
           case "ebsd_65"
            %Al-6.5: 1,96 - 2,10 : 2,03
            ys = [0.6767];
            %H = [38.77];
            al = [0.7185];
            W = [0.866];
           case "ebsd_80"
            %Al-8:   2,30 - 2,43 : 2,36
            ys = [0.7867];
            %H = [68.39];
            al = [0.8361];
            W = [0.866];
       end
    case 3
       % 2800W
       switch regionname
           case "ebsd_20"
            %Al-2:   1,71 - 1,83 : 1,77
            ys = [0.5900];
            %H = [42.21];
            al = [0.00];
            W = [0.933];
           case "ebsd_35"
            %Al-3.5: 1,95 - 2,05 : 2,00
            ys = [0.6667];
            %H = [39.68];
            al = [0.1667];
            W = [0.933];
           case "ebsd_50"
            %Al-5:   1,91 - 2,03 : 1,97
            ys = [0.6567];
            %H = [37.56];
            al = [0.3053];
            W = [0.933];
           case "ebsd_65"
            %Al-6.5: 2,10 - 2,25 : 2,17
            ys = [0.7233];
            %H = [41.69];
            al = [0.5882];
            W = [0.933];;
           case "ebsd_80"
            %Al-8:   2,55 - 2,69 : 2,62
            ys = [0.8733];
            %H = [49.65];
            al = [0.8571];
            W = [0.933];
       end
    case 4
       % 2800W
       switch regionname
           case "ebsd_20"
            %Al-2:   1,71 - 1,83 : 1,77
            ys = [0.5900];
            %H = [42.21];
            al = [0.0392];
            W = [0.933];
           case "ebsd_35"
            %Al-3.5: 1,95 - 2,05 : 2,00
            ys = [0.6667];
            %H = [39.68];
            al = [0.0742];
            W = [0.933];
           case "ebsd_50"
            %Al-5:   1,91 - 2,03 : 1,97
            ys = [0.6567];
            %H = [37.56];
            al = [0.3992];
            W = [0.933];
           case "ebsd_65"
            %Al-6.5: 2,10 - 2,25 : 2,17
            ys = [0.7233];
            %H = [41.69];
            al = [0.5322];
            W = [0.933];
           case "ebsd_80"
            %Al-8:   2,55 - 2,69 : 2,62
            ys = [0.8733];
            %H = [49.65];
            al = [0.7885];
            W = [0.933];
       end
    case 5
       % 3000W
       switch regionname
           case "ebsd_20"
            %Al-2:   1,76 - 1,89 : 1,82
            ys = [0.6067];
            %H = [55.88];
            al = [0.0392];
            W = [1.00];
           case "ebsd_35"
            %Al-3.5: 2,02 - 2,15 : 2,08
            ys = [0.6933];
            %H = [47.11];
            al = [0.0742];
            W = [1.00];
           case "ebsd_50"
            %Al-5:   2,01 - 2,13 : 2,07
            ys = [0.6900];
            %H = [43.66];
            al = [0.3992];
            W = [1.00];
           case "ebsd_65"
            %Al-6.5: 1,97 - 2,10 : 2,03
            ys = [0.6767];
            %H = [48.02];
            al = [0.5322];
            W = [1.00];
           case "ebsd_80"
            %Al-8:   2,40 - 2,53 : 2,46
            ys = [0.8200];
            %H = [70.04];
            al = [0.7885];
            W = [1.00];
       end
end

Length = length(grain_area);
ys = zeros(Length,1) + ys;
al = zeros(Length,1) + al;
W = zeros(Length,1) + W;

fprintf('Create '+filename+'_' + regionname + '.txt\n');
A = [grain_area grain_ar grain_size gam W al ys];
A = rmmissing(A);

   %% Switch Cases für Anzahl der Pakete
    %Alles in eine Line mit Label am Ende. Pakete von 60 oder 100 Körnern.
    %Mehr Daten möglich durch nebeneinander liegende EBSDs bei 2600W, 3000W
    %Split Matrix B = A(1:60,:)   C = A(61:120,:)
    %{
    f = floor(length(A)/60); % Anzahl Splits mit voller Zeilenzahl (60)
    matrices = cell(f,1);
    switch f
        case 1            
            matrices{1,1} = A(1:60,:);
        case 2
            matrices{1,1} = A(1:60,:);
            matrices{2,1} = A(61:120,:);
        case 3
            matrices{1,1} = A(1:60,:);
            matrices{2,1} = A(61:120,:);
            matrices{3,1} = A(121:180,:);
        case 4
            matrices{1,1} = A(1:60,:);
            matrices{2,1} = A(61:120,:);
            matrices{3,1} = A(121:180,:);
            matrices{4,1} = A(181:240,:);
        case 5
            matrices{1,1} = A(1:60,:);
            matrices{2,1} = A(61:120,:);
            matrices{3,1} = A(121:180,:);
            matrices{4,1} = A(181:240,:);
            matrices{5,1} = A(241:300,:);
        case 6
            matrices{1,1} = A(1:60,:);
            matrices{2,1} = A(61:120,:);
            matrices{3,1} = A(121:180,:);
            matrices{4,1} = A(181:240,:);
            matrices{5,1} = A(301:360,:);
        case 7
            matrices{1,1} = A(1:60,:);
            matrices{2,1} = A(61:120,:);
            matrices{3,1} = A(121:180,:);
            matrices{4,1} = A(181:240,:);
            matrices{5,1} = A(301:360,:);
            matrices{6,1} = A(361:420,:);
        case 8
            matrices{1,1} = A(1:60,:);
            matrices{2,1} = A(61:120,:);
            matrices{3,1} = A(121:180,:);
            matrices{4,1} = A(181:240,:);
            matrices{5,1} = A(241:300,:);
            matrices{6,1} = A(301:360,:);
            matrices{7,1} = A(361:420,:);
            matrices{8,1} = A(421:480,:);
        case 9
            matrices{1,1} = A(1:60,:);
            matrices{2,1} = A(61:120,:);
            matrices{3,1} = A(121:180,:);
            matrices{4,1} = A(181:240,:);
            matrices{5,1} = A(241:300,:);
            matrices{6,1} = A(301:360,:);
            matrices{7,1} = A(361:420,:);
            matrices{8,1} = A(421:480,:);
            matrices{9,1} = A(481:540,:);
        case 10
            matrices{1,1} = A(1:60,:);
            matrices{2,1} = A(61:120,:);
            matrices{3,1} = A(121:180,:);
            matrices{4,1} = A(181:240,:);
            matrices{5,1} = A(241:300,:);
            matrices{6,1} = A(301:360,:);
            matrices{7,1} = A(361:420,:);
            matrices{8,1} = A(421:480,:);
            matrices{9,1} = A(481:540,:);
            matrices{10,1} = A(541:600,:);
        case 11
            matrices{1,1} = A(1:60,:);
            matrices{2,1} = A(61:120,:);
            matrices{3,1} = A(121:180,:);
            matrices{4,1} = A(181:240,:);
            matrices{5,1} = A(241:300,:);
            matrices{6,1} = A(301:360,:);
            matrices{7,1} = A(361:420,:);
            matrices{8,1} = A(421:480,:);
            matrices{9,1} = A(481:540,:);
            matrices{10,1} = A(541:600,:);
            matrices{11,1} = A(601:660,:);
        case 12
            matrices{1,1} = A(1:60,:);
            matrices{2,1} = A(61:120,:);
            matrices{3,1} = A(121:180,:);
            matrices{4,1} = A(181:240,:);
            matrices{5,1} = A(241:300,:);
            matrices{6,1} = A(301:360,:);
            matrices{7,1} = A(361:420,:);
            matrices{8,1} = A(421:480,:);
            matrices{9,1} = A(481:540,:);
            matrices{10,1} = A(541:600,:);
            matrices{11,1} = A(601:660,:);
            matrices{12,1} = A(661:720,:);
        case 13
            matrices{1,1} = A(1:60,:);
            matrices{2,1} = A(61:120,:);
            matrices{3,1} = A(121:180,:);
            matrices{4,1} = A(181:240,:);
            matrices{5,1} = A(241:300,:);
            matrices{6,1} = A(301:360,:);
            matrices{7,1} = A(361:420,:);
            matrices{8,1} = A(421:480,:);
            matrices{9,1} = A(481:540,:);
            matrices{10,1} = A(541:600,:);
            matrices{11,1} = A(601:660,:);
            matrices{12,1} = A(661:720,:);
            matrices{13,1} = A(721:780,:);
        case 14
            matrices{1,1} = A(1:60,:);
            matrices{2,1} = A(61:120,:);
            matrices{3,1} = A(121:180,:);
            matrices{4,1} = A(181:240,:);
            matrices{5,1} = A(241:300,:);
            matrices{6,1} = A(301:360,:);
            matrices{7,1} = A(361:420,:);
            matrices{8,1} = A(421:480,:);
            matrices{9,1} = A(481:540,:);
            matrices{10,1} = A(541:600,:);
            matrices{11,1} = A(601:660,:);
            matrices{12,1} = A(661:720,:);
            matrices{13,1} = A(721:780,:);
            matrices{14,1} = A(781:840,:);     
        case 15
            matrices{1,1} = A(1:60,:);
            matrices{2,1} = A(61:120,:);
            matrices{3,1} = A(121:180,:);
            matrices{4,1} = A(181:240,:);
            matrices{5,1} = A(241:300,:);
            matrices{6,1} = A(301:360,:);
            matrices{7,1} = A(361:420,:);
            matrices{8,1} = A(421:480,:);
            matrices{9,1} = A(481:540,:);
            matrices{10,1} = A(541:600,:);
            matrices{11,1} = A(601:660,:);
            matrices{12,1} = A(661:720,:);
            matrices{13,1} = A(721:780,:);
            matrices{14,1} = A(781:840,:);
            matrices{15,1} = A(841:900,:);
        case 16
            matrices{1,1} = A(1:60,:);
            matrices{2,1} = A(61:120,:);
            matrices{3,1} = A(121:180,:);
            matrices{4,1} = A(181:240,:);
            matrices{5,1} = A(241:300,:);
            matrices{6,1} = A(301:360,:);
            matrices{7,1} = A(361:420,:);
            matrices{8,1} = A(421:480,:);
            matrices{9,1} = A(481:540,:);
            matrices{10,1} = A(541:600,:);
            matrices{11,1} = A(601:660,:);
            matrices{12,1} = A(661:720,:);
            matrices{13,1} = A(721:780,:);
            matrices{14,1} = A(781:840,:);
            matrices{15,1} = A(841:900,:);
            matrices{16,1} = A(901:960,:);
        case 17
            matrices{1,1} = A(1:60,:);
            matrices{2,1} = A(61:120,:);
            matrices{3,1} = A(121:180,:);
            matrices{4,1} = A(181:240,:);
            matrices{5,1} = A(241:300,:);
            matrices{6,1} = A(301:360,:);
            matrices{7,1} = A(361:420,:);
            matrices{8,1} = A(421:480,:);
            matrices{9,1} = A(481:540,:);
            matrices{10,1} = A(541:600,:);
            matrices{11,1} = A(601:660,:);
            matrices{12,1} = A(661:720,:);
            matrices{13,1} = A(721:780,:);
            matrices{14,1} = A(781:840,:);
            matrices{15,1} = A(841:900,:);
            matrices{16,1} = A(901:960,:);
            matrices{17,1} = A(961:1020,:);
         case 23
            matrices{1,1} = A(1:60,:);
            matrices{2,1} = A(61:120,:);
            matrices{3,1} = A(121:180,:);
            matrices{4,1} = A(181:240,:);
            matrices{5,1} = A(241:300,:);
            matrices{6,1} = A(301:360,:);
            matrices{7,1} = A(361:420,:);
            matrices{8,1} = A(421:480,:);
            matrices{9,1} = A(481:540,:);
            matrices{10,1} = A(541:600,:);
            matrices{11,1} = A(601:660,:);
            matrices{12,1} = A(661:720,:);
            matrices{13,1} = A(721:780,:);
            matrices{14,1} = A(781:840,:);
            matrices{15,1} = A(841:900,:);
            matrices{16,1} = A(901:960,:);
            matrices{17,1} = A(961:1020,:);
            matrices{18,1} = A(1021:1080,:);
            matrices{19,1} = A(1081:1140,:);
            matrices{20,1} = A(1141:1200,:);
            matrices{21,1} = A(1201:1260,:);
            matrices{22,1} = A(1261:1320,:);
            matrices{23,1} = A(1321:1380,:);
        case 24
            matrices{1,1} = A(1:60,:);
            matrices{2,1} = A(61:120,:);
            matrices{3,1} = A(121:180,:);
            matrices{4,1} = A(181:240,:);
            matrices{5,1} = A(241:300,:);
            matrices{6,1} = A(301:360,:);
            matrices{7,1} = A(361:420,:);
            matrices{8,1} = A(421:480,:);
            matrices{9,1} = A(481:540,:);
            matrices{10,1} = A(541:600,:);
            matrices{11,1} = A(601:660,:);
            matrices{12,1} = A(661:720,:);
            matrices{13,1} = A(721:780,:);
            matrices{14,1} = A(781:840,:);
            matrices{15,1} = A(841:900,:);
            matrices{16,1} = A(901:960,:);
            matrices{17,1} = A(961:1020,:);
            matrices{18,1} = A(1021:1080,:);
            matrices{19,1} = A(1081:1140,:);
            matrices{20,1} = A(1141:1200,:);
            matrices{21,1} = A(1201:1260,:);
            matrices{22,1} = A(1261:1320,:);
            matrices{23,1} = A(1321:1380,:);
            matrices{24,1} = A(1381:1440,:);
        case 25
            matrices{1,1} = A(1:60,:);
            matrices{2,1} = A(61:120,:);
            matrices{3,1} = A(121:180,:);
            matrices{4,1} = A(181:240,:);
            matrices{5,1} = A(241:300,:);
            matrices{6,1} = A(301:360,:);
            matrices{7,1} = A(361:420,:);
            matrices{8,1} = A(421:480,:);
            matrices{9,1} = A(481:540,:);
            matrices{10,1} = A(541:600,:);
            matrices{11,1} = A(601:660,:);
            matrices{12,1} = A(661:720,:);
            matrices{13,1} = A(721:780,:);
            matrices{14,1} = A(781:840,:);
            matrices{15,1} = A(841:900,:);
            matrices{16,1} = A(901:960,:);
            matrices{17,1} = A(961:1020,:);
            matrices{18,1} = A(1021:1080,:);
            matrices{19,1} = A(1081:1140,:);
            matrices{20,1} = A(1141:1200,:);
            matrices{21,1} = A(1201:1260,:);
            matrices{22,1} = A(1261:1320,:);
            matrices{23,1} = A(1321:1380,:);
            matrices{24,1} = A(1381:1440,:);
            matrices{25,1} = A(1441:1500,:);
        case 34
            matrices{1,1} = A(1:60,:);
            matrices{2,1} = A(61:120,:);
            matrices{3,1} = A(121:180,:);
            matrices{4,1} = A(181:240,:);
            matrices{5,1} = A(241:300,:);
            matrices{6,1} = A(301:360,:);
            matrices{7,1} = A(361:420,:);
            matrices{8,1} = A(421:480,:);
            matrices{9,1} = A(481:540,:);
            matrices{10,1} = A(541:600,:);
            matrices{11,1} = A(601:660,:);
            matrices{12,1} = A(661:720,:);
            matrices{13,1} = A(721:780,:);
            matrices{14,1} = A(781:840,:);
            matrices{15,1} = A(841:900,:);
            matrices{16,1} = A(901:960,:);
            matrices{17,1} = A(961:1020,:);
            matrices{18,1} = A(1021:1080,:);
            matrices{19,1} = A(1081:1140,:);
            matrices{20,1} = A(1141:1200,:);
            matrices{21,1} = A(1201:1260,:);
            matrices{22,1} = A(1261:1320,:);
            matrices{23,1} = A(1321:1380,:);
            matrices{24,1} = A(1381:1440,:);
            matrices{25,1} = A(1441:1500,:);
            matrices{26,1} = A(1501:1560,:);
            matrices{27,1} = A(1561:1620,:);
            matrices{28,1} = A(1621:1680,:);
            matrices{29,1} = A(1681:1740,:);
            matrices{30,1} = A(1741:1800,:);
            matrices{31,1} = A(1801:1860,:);
            matrices{32,1} = A(1861:1920,:);
            matrices{33,1} = A(1921:1980,:);
            matrices{34,1} = A(1981:2040,:);
        case 35
            matrices{1,1} = A(1:60,:);
            matrices{2,1} = A(61:120,:);
            matrices{3,1} = A(121:180,:);
            matrices{4,1} = A(181:240,:);
            matrices{5,1} = A(241:300,:);
            matrices{6,1} = A(301:360,:);
            matrices{7,1} = A(361:420,:);
            matrices{8,1} = A(421:480,:);
            matrices{9,1} = A(481:540,:);
            matrices{10,1} = A(541:600,:);
            matrices{11,1} = A(601:660,:);
            matrices{12,1} = A(661:720,:);
            matrices{13,1} = A(721:780,:);
            matrices{14,1} = A(781:840,:);
            matrices{15,1} = A(841:900,:);
            matrices{16,1} = A(901:960,:);
            matrices{17,1} = A(961:1020,:);
            matrices{18,1} = A(1021:1080,:);
            matrices{19,1} = A(1081:1140,:);
            matrices{20,1} = A(1141:1200,:);
            matrices{21,1} = A(1201:1260,:);
            matrices{22,1} = A(1261:1320,:);
            matrices{23,1} = A(1321:1380,:);
            matrices{24,1} = A(1381:1440,:);
            matrices{25,1} = A(1441:1500,:);
            matrices{26,1} = A(1501:1560,:);
            matrices{27,1} = A(1561:1620,:);
            matrices{28,1} = A(1621:1680,:);
            matrices{29,1} = A(1681:1740,:);
            matrices{30,1} = A(1741:1800,:);
            matrices{31,1} = A(1801:1860,:);
            matrices{32,1} = A(1861:1920,:);
            matrices{33,1} = A(1921:1980,:);
            matrices{34,1} = A(1981:2040,:);
            matrices{35,1} = A(2041:2100,:);
        case 36
            matrices{1,1} = A(1:60,:);
            matrices{2,1} = A(61:120,:);
            matrices{3,1} = A(121:180,:);
            matrices{4,1} = A(181:240,:);
            matrices{5,1} = A(241:300,:);
            matrices{6,1} = A(301:360,:);
            matrices{7,1} = A(361:420,:);
            matrices{8,1} = A(421:480,:);
            matrices{9,1} = A(481:540,:);
            matrices{10,1} = A(541:600,:);
            matrices{11,1} = A(601:660,:);
            matrices{12,1} = A(661:720,:);
            matrices{13,1} = A(721:780,:);
            matrices{14,1} = A(781:840,:);
            matrices{15,1} = A(841:900,:);
            matrices{16,1} = A(901:960,:);
            matrices{17,1} = A(961:1020,:);
            matrices{18,1} = A(1021:1080,:);
            matrices{19,1} = A(1081:1140,:);
            matrices{20,1} = A(1141:1200,:);
            matrices{21,1} = A(1201:1260,:);
            matrices{22,1} = A(1261:1320,:);
            matrices{23,1} = A(1321:1380,:);
            matrices{24,1} = A(1381:1440,:);
            matrices{25,1} = A(1441:1500,:);
            matrices{26,1} = A(1501:1560,:);
            matrices{27,1} = A(1561:1620,:);
            matrices{28,1} = A(1621:1680,:);
            matrices{29,1} = A(1681:1740,:);
            matrices{30,1} = A(1741:1800,:);
            matrices{31,1} = A(1801:1860,:);
            matrices{32,1} = A(1861:1920,:);
            matrices{33,1} = A(1921:1980,:);
            matrices{34,1} = A(1981:2040,:);
            matrices{35,1} = A(2041:2100,:);
            matrices{36,1} = A(2101:2160,:);
    end

    %%Write File
    fileID = fopen(filename+ '/' + filename + '_' + regionname + '.txt','w');
    fprintf(fileID,'%12.8f\t %11.8f\t %11.8f\t %12.8f\t %12.8f\t %12.8f\t %12.8f\t\n',transpose(A));
    fclose(fileID);



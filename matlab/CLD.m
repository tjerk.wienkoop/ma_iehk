%% Import Script for EBSD Data
%
% This script was automatically created by the import wizard. You should
% run the whoole script or parts of it in order to import your data. There
% is no problem in making any changes to this script.

%% Specify Crystal and Specimen Symmetries

% crystal symmetry
%CS = {... 
%  'notIndexed',...
%  crystalSymmetry('m-3m', [3.7 3.7 3.7], 'mineral', 'Iron fcc', 'color', [0.53 0.81 0.98]),...
%  crystalSymmetry('m-3m', [2.9 2.9 2.9], 'mineral', 'Iron bcc (old)', 'color', [0.56 0.74 0.56]),...
%  crystalSymmetry('6/mmm', [2.5 2.5 4.1], 'X||a*', 'Y||b', 'Z||c*', 'mineral', 'Epsilon-Martensit', 'color', [0.85 0.65 0.13])};

% plotting convention
setMTEXpref('xAxisDirection','north');
setMTEXpref('zAxisDirection','outOfPlane');

%% Specify File Names

% path to files
%pname = '';

% which files to be imported
%fname = [pname '\200°C.ctf'];

%% Import the Data

% create an EBSD variable containing the data
%ebsd = EBSD.load(fname,CS,'interface','ctf',...
%  'convertEuler2SpatialReferenceFrame');



%% cleaning data

histogram(ebsd.mad); % oder ebsd.ci (confidence index)

ebsd_corrected=ebsd(ebsd.mad<3); % BSP: ebsd Daten mit mad<0.8 sind in ebsd_corrected enthalten

%% grain calculation
% 
% % einfache GWKG Bestimmung
% [grains10,ebsd_corrected('indexed').grainId]=calcGrains(ebsd_corrected('indexed'),'angle',10*degree); % 10? misorientierung zur KG Bestimmung; nur indexed zur Bestimmung verwenden
% 
% %Subkorngrenzen 
% grains2=calcGrains(ebsd_corrected('indexed'),'angle',2*degree);
% 
% % f?r smoothing ben?tigt
% [grains,ebsd_corrected.grainId,ebsd_corrected.mis2mean]=calcGrains(ebsd_corrected('indexed'),'angle',3*degree); % f?r smoothing ben?tigt, 3-8? gute Werte
% 
% % Falls sehr kleine K?rner in den Abbildungen enthalten, so kann man diese
% % entfernen
% grains_red=grains10(grains10.grainSize>=10);
% 
% % twin detection; phase sollte die fcc phase sein
% gB=grains.boundary('Iron fcc','Iron fcc');
% gB3=gB(angle(gB.misorientation,CSL(3,ebsd('Iron fcc').CS))<5*degree); % gB3 variable mit zwillingsgrenzen
% 
% % grainboundary smoothing (Faktor 2-4)
% grains10_smoothed=smooth(grains10,3);
% grains2_smoothed=smooth(grains2,3);

%% grain calculation

% einfache GWKG Bestimmung
[grains10,ebsd_corrected('indexed').grainId]=calcGrains(ebsd_corrected('indexed'),'angle',10*degree); % 10? misorientierung zur KG Bestimmung; nur indexed zur Bestimmung verwenden
%[grains,ebsd_corrected.grainId,ebsd_corrected.mis2mean]=calcGrains(ebsd_corrected('indexed'),'angle',3*degree); % f?r smoothing ben?tigt, 3-8? gute Werte
grains10_smoothed=smooth(grains10,3);
%% smoothing data

% types of filter

% automatische kalibrierung, aber erst in K?rnern smoothen, dann die
% Korngrenzen
%F=splineFilter;

% smoothing inside a grain
%ebsd_smoothed=smooth(ebsd_corrected('indexed'),F,'fill',grains10_smoothed);

% smoothing on grain boundaries
%ebsd_smoothed=smooth(ebsd_smoothed('indexed'),F,'fill');



% standard Channel5 Filter
% F=medianFilter;
% F.numNeighbours=2; %5x5 window

%ebsd_smoothed=smooth(ebsd_corrected('indexed'),F,'fill');



%% plotting data
%{
%parameter
setMTEXpref('xAxisDirection','west');
setMTEXpref('zAxisDirection','intoPlane');
setMTEXpref('showMicronBar','off');

%chemie
setMTEXpref('xAxisDirection','east');
setMTEXpref('zAxisDirection','intoPlane');
% setMTEXpref('showMicronBar','off');

% phase map
figure;
%plot(ebsd); % ausgangsdaten
plot(ebsd_smoothed('indexed')); % smoothed
legend off;

% bandcontrast
figure;
plot(ebsd,ebsd.bc);
%plot(ebsd,ebsd.iq);
colormap gray %schwarz-wei? darstellung


% IPF overlay
oM=ipdfHSVOrientationMapping(ebsd_smoothed('Iron fcc'));
oM.inversePoleFigureDirection=xvector;
color=oM.orientation2color(ebsd_smoothed('Iron fcc').orientations);

oM2=ipdfHSVOrientationMapping(ebsd_smoothed('Iron bcc'));
oM2.inversePoleFigureDirection=yvector;
color2=oM2.orientation2color(ebsd_smoothed('Iron bcc').orientations);

oM3=ipdfHSVOrientationMapping(ebsd_smoothed('Epsilon-Martensit'));
oM3.inversePoleFigureDirection=yvector;
color3=oM3.orientation2color(ebsd_smoothed('Epsilon-Martensit').orientations);

figure;
plot(ebsd_smoothed('Iron fcc'),color); %ipf colorcoding
hold on;
plot(ebsd_smoothed('Iron bcc'),color2);
figure;
plot(oM); %orientation map

% all grain boundaries
hold on;
% plot(grains2.boundary,'linewidth',0.5,'linecolor','blue'); 
% plot(grains10.boundary,'linewidth',0.5,'linecolor','black'); 
% plot(grains_red.boundary,'linewidth',1,'color','black');
% plot(gB3, 'linecolor', 'white', 'displayname', 'Sigma3');

% grain boundary between phases
hold on
plot(grains10_smoothed.boundary('Iron fcc','Iron fcc'),'linewidth',1,'color','black');

%local misorientation with KAM
% kam = KAM(ebsd_smoothed,'threshold',2*degree); % ignore misorientation angles > threshold, consider neigbors of order n
% gnd = (2*kam)/(b*700E-9);
% plot(ebsd_smoothed,gnd);
% mtexColorbar;
% mtexColorMap viridis;
% set(gca,'ColorScale','log');
% CLim(gcm,[1E13 1E15]);
% 
% hist(kam./degree);

%calculate GNDs
% ebsd2 = ebsd_smoothed('Iron fcc').gridify;
% gnd = calcGND(ebsd2);
% plot(ebsd2,gnd);
% mtexColorbar;
% mtexColorMap viridis;
% CLim(gcm,[0 0.1]);

%gnd = (2*kam)/(b*700E-9);
%mean(gnd(:),'omitnan');
%histogram(gnd);
%set(gca, 'xlim',[0 4E14]);
%}
%%
gbs= gb2gbs(grains10_smoothed.boundary);
%%
dxy = max(ebsd.unitCell) - min(ebsd.unitCell);
spacing = min(dxy); %spacing between the test lines
angs = 0:15:179; % number of angles
cho_max = 0.5*max(grains10_smoothed.diameter); %maximum grain size comsidered in CLD
%%
stats = calc_CLD(gbs,spacing,angs,'cho_max',cho_max);
%%
%figure
cld_full = [stats.CLD;stats.CLD(1:end,:)];
%plot_CLD(stats.hst_x,cld_full)

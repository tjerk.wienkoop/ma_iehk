%% Lesen der Phasen aus der .ctf Datei. Wobei die .ctf in eine .txt gewandelt wurde.
% Funktioniert solange der Header der .ctf Datei so bleibt wie bisher
% - 20210508
%https://javahungry.blogspot.com/2020/04/could-not-reserve-enough-space-object-heap.html
%8GB geht
fprintf('Parse CS\n');
fid = fopen(n);

%X- & Y-Cells in z.4,5
XY = textscan(fid, '%*s %d', 2, 'headerLines', 4);
% KV und working distance in z.12
kvwd = textscan(fid, '%*s %*s %*f %*s %*f %*s %*f %s %f %*s %*f %*s %*f %*s %*f %*s %*f %*s %*f %s %f %*s %*f', 1, 'headerLines', 6, 'Delimiter',';\t');

% Anzahl Phasen m aus Zeile 13
phasesCount = cell2mat(textscan(fid, '%*s %d', 1,'headerLines', 1));
m = phasesCount;

%Phasenwinkel und Namen für k Phasen
angleNames = textscan(fid, '%f %f %f %*f %*f %*f %s %*[^\n]', m, 'Delimiter',';\t');
fclose(fid);
